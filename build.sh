#!/bin/bash

# Get old version for be used as cache
docker pull $CI_REGISTRY_IMAGE:"${PYTHON_VERSION}" || true

# Build it from cache
docker build --cache-from $CI_REGISTRY_IMAGE:"${PYTHON_VERSION}" \
--build-arg="PYTHON_VERSION=${PYTHON_VERSION}"  \
--tag $CI_REGISTRY_IMAGE:"${PYTHON_VERSION}" \
-f $DOCKER_FILE .

# Push images
docker push $CI_REGISTRY_IMAGE:"${PYTHON_VERSION}"
