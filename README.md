# Python base image
This docker image is configured based on docker best practices and used it as python base image for all python projects.

This project create a new image weekly on Tuesday at 7 PM for new python, pip and poetry updates.

* The python updates just apply for the version 3.11.x

# Python versions supported
- 3.11.4

## Installation
Just need to add `registry.gitlab.com/fairplay2/open-source/docker/python:3.11` into your dockerfile file.

```docker
FROM registry.gitlab.com/fairplay2/open-source/docker/python:3.11
```

After you install all your dependencies you need to switch the user for more protection with:

```docker
USER appuser
```

So your Dockerimage file must looks like:

```docker
FROM registry.gitlab.com/fairplay2/open-source/docker/python:3.11

WORKDIR /app
COPY ./src /app

RUN pip install -r requirements.txt

USER appuser
EXPOSE 9000
CMD ["python", "manage.py", "runserver", "0.0.0.0:9000"]
```

## Maintainers
<table>
    <tr>
        <td align="center">
            <a href="https://gitlab.com/dharwinfairplay">
                <img src="https://gitlab.com/uploads/-/system/user/avatar/12814508/avatar.png?width=400" width="100px;" alt="Dharwin Perez"/>
                <br />
                <sub>
                    <b>Dharwin Perez</b>
                </sub>
            </a>
        </td>
    </tr>
</table>

## Contributors ✨
<table>
    <tr>
        <!-- <td align="center">
            <a href="https://gitlab.com/dharwinfairplay">
                <img src="https://gitlab.com/uploads/-/system/user/avatar/12814508/avatar.png?width=400" width="100px;" alt="Dharwin Perez"/>
                <br />
                <sub>
                    <b>Dharwin Perez</b>
                </sub>
            </a>
        </td> -->
    </tr>
</table>